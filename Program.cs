﻿using Dapper;
using System;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Threading;
using System.Linq;

namespace EFTest
{
    class Program
    {
        public const string CS = "Data Source=(local); Database=Scratch; Trusted_Connection=true";

        static void Main(string[] args)
        {
            using (var sql = new SqlConnection(CS))
            {
                sql.Execute("DROP TABLE IF EXISTS HasIdentity");
                sql.Execute(@"CREATE TABLE HasIdentity (
					id int primary key identity(1,1),
					name nvarchar(100) null,
					HasDefault datetime default (getdate())
				)");

                sql.Execute("DROP TABLE IF EXISTS None");
                sql.Execute(@"CREATE TABLE None (
					id int primary key identity(1,1),
					name nvarchar(100) null,
					HasDefault datetime default (getdate())
				)");
            }

            using (var db = new MyContext())
            {
                var sql = db.Database.GetDbConnection();

                Console.WriteLine("Inserting new HasIdentity with no values set");
                Execute(() =>
                {
                    db.HasIdentity.Add(new HasIdentity());
                    db.SaveChanges();
                });
                Dump(sql);

                Thread.Sleep(1000); // make sure one second elapses

                Console.WriteLine("Changing the name only");
                Execute(() =>
                {
                    var row = db.HasIdentity.Single();
                    row.Name = "updated 1";
                    db.SaveChanges();
                });
                Dump(sql);

                Console.WriteLine("Changing the name and date");
                Execute(() =>
                {
                    var row = db.HasIdentity.Single();
                    row.Name = "updated 2";
                    row.HasDefault = row.HasDefault.AddSeconds(10);
                    db.SaveChanges();
                });
                Dump(sql);
            }
        }

        static void Execute(Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                Console.WriteLine("EXCEPTION: " + ex.Message);
                if (ex.InnerException != null)
                {
                    Console.WriteLine("INNER EXCEPTION: " + ex.InnerException.Message);
                }
                Environment.Exit(1);
            }
        }

        static void Dump(IDbConnection sql)
        {
            Console.WriteLine("Rows in HasIdentity table:");
            foreach (var row in sql.Query("SELECT * FROM HasIdentity"))
            {
                Console.WriteLine(row);
            }
            Console.WriteLine("Rows in None table:");
            foreach (var row in sql.Query("SELECT * FROM None"))
            {
                Console.WriteLine(row);
            }
        }
    }

    [Table("HasIdentity")]
    public class HasIdentity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "datetime")]
        public DateTime HasDefault { get; set; }

        // [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        // [Column(TypeName = "datetime")]
        // public DateTime NoDefault { get; set; }
    }

    [Table("None")]
    public class None
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime HasDefault { get; set; }
        // [Column(TypeName = "datetime")]
        // public DateTime NoDefault { get; set; }
    }

    public class MyContext : DbContext
    {
        public DbSet<HasIdentity> HasIdentity { get; set; }
        public DbSet<None> None { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(Program.CS);
        }
    }

}
